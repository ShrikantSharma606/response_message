package example.controller;

import example.Application;
import example.model.Person;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;


@SpringBootTest(classes = Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PersonControllerTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private PersonController personController;
    private MockMvc mockMvc;

    @LocalServerPort
    private int port;

    @BeforeClass
    public void setup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        RestAssured.port = port;
    }

    @Test(enabled = false)
    public void testEmployee() throws Exception {
        System.out.println("Thread Id :" + Thread.currentThread().getId());
//        mockMvc.perform(get("/1"))
//                .andDo(print()).andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.id").value("1"))
//                .andExpect(jsonPath("$.firstName").value("1 first name"))
//                .andExpect(jsonPath("$.lastName").value("1 last name"));
    }

    @Test(enabled = false)
    public void testGetAllPerson() throws Exception {
        System.out.println("PersonControllerTest.testGetAllPerson");
//        mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @Test(enabled = false)
    public void testPutPerson() throws Exception {
        System.out.println("PersonControllerTest.testPutPerson");
//        mockMvc.perform(delete("?personNo=1"))
//                .andDo(print())
//                .andExpect(status().isOk())
//                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
//                .andExpect(jsonPath("$.firstName").exists());

    }

    @Test(enabled = false)
    public void testGetPerson() {

    }

    @Test(enabled = false)
    public void testTestGetAllPerson() {
    }

    @Test(enabled = false, testName = "Person Schema Validation")
    public void getPersonSchemaValidation() {
        Response response = when().get("/1");
        response.then().assertThat().body(io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath("person-schema.json")).log();
        System.out.println(response.toString());
        System.out.println("Time Taken in Milisecounds" + response.then().extract().timeIn(TimeUnit.MILLISECONDS));
    }

    @Test(enabled = false, testName = "verify that lottoId is equal to 5")
    public void testTestRestAssured1() {
        Response response = when().get("/lotto");
        response.then().body("lotto.lottoId", org.hamcrest.Matchers.equalTo(5)).log();
        System.out.println(response.toString());
        System.out.println("Time Taken in Milisecounds" + response.then().extract().timeIn(TimeUnit.MILLISECONDS));
    }

    @Test(enabled = false, testName = "check that the winnerId's are 23 and 54")
    public void testTestRestAssured2() {
        Response response = when().get("/lotto");
        response.then().body("lotto.winners.winnerId", org.hamcrest.Matchers.hasItems(23, 54));
        System.out.println(response.toString());
        System.out.println("Time Taken in Milisecounds" + response.then().extract().timeIn(TimeUnit.MILLISECONDS));
    }

    @Test(enabled = false, testName = "price is float")
    public void testTestRestAssured3() {
        Response response = when().get("/lotto");
        response.then().body("lotto.price", org.hamcrest.Matchers.is(12.12f));
        System.out.println(response.toString());
        System.out.println("Time Taken in Milisecounds" + response.then().extract().timeIn(TimeUnit.MILLISECONDS));
    }

    @Test(enabled = false, testName = "pass parameter in api")
    public void testTestRestAssured4() {
        given()
                .param("personId", 1)
                .when()
                .get("/")
                .then()
                .assertThat().statusCode(HttpStatus.OK.value()).log();
    }

    // parse json using json path and get data from it. then call another api with that data.
    @Test(testName = "get data from 1 api, put in another", enabled = false)
    public void getFrom1PassIntoAnother() {
        Response response = given()
                .when()
                .get("/");
        System.out.println("response = " + response.toString());
        int personId = response.then()
                .contentType(ContentType.JSON)
                .extract().path("[0].id");
        System.out.println("personId = " + personId);
        System.out.println("Time Taken in Milisecounds" + response.then().extract().timeIn(TimeUnit.MILLISECONDS));
    }

    @Test(testName = "add a new person via json", enabled = false)
    public void testTestPutPerson() {
        Response response = given().body("{\n" +
                "    \"12\": {\n" +
                "        \"id\": 12,\n" +
                "        \"firstName\": \"1 first name12\",\n" +
                "        \"lastName\": \"1 last name\"\n" +
                "    }\n" +
                "}")
                .when().contentType(ContentType.JSON)
                .put("/");
        System.out.println("response = " + response.statusCode());
        response = when().get("/12");
        response.then().assertThat().body(io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath("person-schema.json")).log();
        System.out.println("print new created data " + response.body().prettyPrint());
        System.out.println("Time Taken in Milisecounds" + response.then().extract().timeIn(TimeUnit.MILLISECONDS));
    }

    @Test(testName = "add a new person via obj", enabled = false)
    public void testTestPutPerson1() {
        Map PERSON_MAP = new HashMap<>();
        PERSON_MAP.put(11, new Person(11, "11 first name", "11 last name"));
        Response response = given().body(PERSON_MAP)
                .when().contentType(ContentType.JSON)
                .put("/");
        System.out.println("response = " + response.statusCode());
        response = when().get("/11");
        response.then().assertThat().body(io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath("person-schema.json")).log();
        System.out.println("print new created data " + response.body().prettyPrint());
        System.out.println("Time Taken in Milisecounds" + response.then().extract().timeIn(TimeUnit.MILLISECONDS));
    }

    @Test(enabled = false)
    public void testPostPerson() {
    }

    @Test(testName = "delete person")
    public void testDeletePerson() {
        Response response = given().param("personNo", 1)
                .when()
                .delete("/");
        System.out.println("response.statusCode() = " + response.statusCode());
        System.out.println("Time Taken in Milisecounds" + response.then().extract().timeIn(TimeUnit.MILLISECONDS));

    }
}