package example.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {
    @GetMapping(value = "/")
    public ResponseEntity<?> test() {
        return new ResponseEntity<>("hello", HttpStatus.OK);
    }

    @GetMapping(value = "/success")
    public ResponseEntity<?> success() {
        Response response = new Response("shrikant", "sharma");
        return MyAirtelAppRequestUtils.sendSuccessOkResponse(response);
    }

    @GetMapping(value = "/badreq")
    public ResponseEntity<?> badreq() {
        return MyAirtelAppRequestUtils.sendErrorResponse(MyAirtelAppResponseCodes.BAD_REQUEST, MyAirtelAppResponseCodes.BAD_REQUEST);
    }

    @GetMapping(value = "/error")
    public ResponseEntity<?> error() {
        return MyAirtelAppRequestUtils.sendErrorResponse(MyAirtelAppResponseCodes.ERROR_PROCESSING_REQUEST);
    }
}